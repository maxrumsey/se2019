// Requiring Dependencies
const { ipcRenderer, Menu } = require('electron');
const electronPrompt = require('electron-prompt');

global.exit = function () {
  ipcRenderer.send('close-me')
}
global.guess = async function() {
  const number = Math.random() * 100 + 1;
  while (true) {
    const guess = await prompt('Enter a number.')
    if (isNaN)
  }
}
global.kebab = async function() {
  const orders = []
  for (var i = 0; i < 5; i++) {
    let order = await prompt('What flavour of kebab do you want? (Chicken or lamb.)');
    order = order.toLowerCase()
    if (order !== "chicken" && order !== "lamb") return alert('Order not chicken or lamb.')
    orders.push(order)
  }
  alert(`Orders:\n${orders.join('\n')}`)
}
global.area = async function() {
  const shape = (await prompt('What shape do you want to calculate the area of?')).toLowerCase();
  const width = await prompt('Enter the length of your shape (or radius of circle)');
  const height = await prompt('Enter the width of your shape (or radius of circle)');
  if (isNaN(width) || isNaN(height)) return alert('Ensure all values entered are numbers')

  let area;
  if (shape === 'triangle') {
    area = width * (height * 0.5);
  } else if (shape === 'square') {
    area = width * height;
  } else if (shape === 'circle') {
    area = Math.PI * (width ** 2)
  } else {
    return alert('Unknown Shape');
  }
  alert(`You calculated the area of a ${shape} and the program outputted ${area.toFixed(2)}.`)
}

function prompt(msg) {
  return new Promise((resolve, reject) => {
    electronPrompt({
      title: 'Enter a Value',
      label: msg,
      value: 'Enter a Value',
    })
    .then((r) => {
      if(r === null) {
        reject()
      } else {
        resolve(r)
      }
    })
    .catch(e => reject(console.error(e)));

  })
}
