# Currency Converter
An application that converts a dollar amount in AUD (Australian Dollar) into various other currencies. Requires a free [Fixer.Io](Fixer.io) API key.
