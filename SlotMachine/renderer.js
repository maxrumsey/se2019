// Requiring Dependencies
const { ipcRenderer } = require('electron');
const imageSrc = {
  cherry: 5000,
  grape: 5000,
  bell: 3000,
  diamond: 3000,
  clover: 3000,
  lemon: 3000,
  coin: 2500,
  bar: 250,
  apple: 250,
  plum: 2,
  shoe: 2,
  seven: 2,
  watermelon: 2,
  heart: 1,
  orange: 1
}
imageRarity = {
  cherry: 5,
  grape: 5,
  bell: 8,
  diamond: 8,
  clover: 4,
  lemon: 4,
  coin: 10,
  bar: 20,
  apple: 20,
  plum: 25,
  shoe: 25,
  seven: 25,
  watermelon: 25,
  heart: 50,
  orange: 50
}
const TIME_TO_SPIN = 2000;
const IMG_LOOP_TIME = 25;
const MULTIPLIER = 7;
const DEFAULTCOST = 25;

let runningTotal = 0;
let losses = 0;
let wins = 0;

setText(runningTotal, 'runningTotal')
setText(wins, 'wins')
setText(losses, 'losses')

global.exit = function () {
  ipcRenderer.send('close-me')
}
global.run = function() {
  const betModifierString = document.getElementById('input').value;
  const betModifier = parseInt(betModifierString);

  if (isNaN(betModifier)) return alert('Bet amount not a valid integer.')
  if (betModifier <= 0) return alert('Bet amount is less then / equal to 0.')

  document.getElementById('run').disabled = true;
  runningTotal -= betModifier;
  setText(runningTotal, 'runningTotal')
  setMsgBox(' ')
  const loadloop = setInterval(function() {
    const array = Object.keys(imageSrc);

    for (var i = 1; i < 16; i++) {
      setImageSource('img/' + getRandFromArr(populateArray(imageRarity)) + '.png', i)
    }
  }, IMG_LOOP_TIME)
  setTimeout(function() {
    clearTimeout(loadloop)
    const array = Object.keys(imageSrc);

    for (var i = 1; i < 16; i++) {
      setImageSource('img/' + getRandFromArr(populateArray(imageRarity)) + '.png', i)
    }
    const line = lineToArr();
    console.log(line)
    const RecurseFuncOutput = []
    function recurse(name, index, amnt) {
      if (index === 5) {
        return RecurseFuncOutput.push({name, amnt})
      }

      if (line[index] == name) {
        return recurse(name, index + 1, amnt + 1)
      } else {
        RecurseFuncOutput.push({name, amnt})
        return recurse(line[index], index + 1, 1)
      }
    }
    recurse(line[0], 1, 1)
    console.log(RecurseFuncOutput)

    const prizes = [];
    for (var i = RecurseFuncOutput.length - 1; i >= 0; i--) {
      if (RecurseFuncOutput[i].amnt === 1) continue;
      else prizes.push(RecurseFuncOutput[i])
    }

    document.getElementById('run').disabled = false;

    if (prizes.length === 0) {
      setMsgBox('Sorry, No Prizes.')
      losses += 1;
      setText(losses, 'losses')
      return console.log('Sorry, No Prizes')
    }
    console.table(prizes);
    let total = 0;
    for (var i = prizes.length - 1; i >= 0; i--) {
      const amtToMultiplyBy = MULTIPLIER ** prizes[i].amnt;
      const money = imageSrc[prizes[i].name] * amtToMultiplyBy;
      total += money;
      console.log(money)
    }
    total = total * (betModifier / 25);
    console.log('Total: ' + total)
    setMsgBox('WINNER! Money Won: $' + total)
    runningTotal += total;
    wins += 1;
    setText(wins, 'wins')
    setText(runningTotal, 'runningTotal')
  }, TIME_TO_SPIN)
}
function setImageSource(src, num) {
  const image = document.getElementById('slot-image-' + num);
  image.src = src;
}
function getRandFromArr(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}
global.populateArray = (src) => {
  const entries = Object.entries(src);
  let arr = [];
  for (var i = 0; i < entries.length; i++) {
    for (var j = 0; j < entries[i][1]; j++) {
      arr.push(entries[i][0])
    }
  }
  return arr;
}
function lineToArr() {
  const arr = [];
  for (var i = 6; i < 11; i++) {
    const src = document.getElementById('slot-image-' + i).src.split('/');
    arr.push(src[src.length - 1].split('.')[0])
  }
  return arr;
}
function setMsgBox(txt) {
  document.getElementById('txt').innerHTML = txt;
}function setText(txt, id) {
  document.getElementById(id).innerHTML = txt;
}
/*
 * Initialise Slots With Images
 */
for (var i = 1; i < 16; i++) {
  setImageSource('img/' + getRandFromArr(populateArray(imageRarity)) + '.png', i)
}