// Requiring Dependencies
const { ipcRenderer, Menu } = require('electron');
const imageSrc = [
  [
    "http://www.clker.com/cliparts/X/w/P/Y/q/H/dice-1-md.png"
  ],
  [
    "http://www.clker.com/cliparts/X/V/S/C/I/x/dice-2-md.png"
  ],
  [
    "http://www.clker.com/cliparts/M/e/P/O/L/b/dice-3-md.png"
  ],
  [
    "http://www.clker.com/cliparts/r/z/d/a/L/V/dice-4-md.png"
  ],
  [
    "http://www.clker.com/cliparts/e/y/7/h/W/K/dice-5-hi.png"
  ],
  [
    "https://is5-ssl.mzstatic.com/image/thumb/Purple118/v4/0c/dd/dc/0cdddc8c-b7c4-411a-1b39-7ceb0ce4265c/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-3.png/246x0w.jpg"
  ]
]

global.exit = function () {
  ipcRenderer.send('close-me')
}
global.run = function() {
  setDec('')
  const dice1 = getRandom();
  const dice2 = getRandom();

  setImageSource(getRandFromArr(imageSrc[dice1 - 1]), '1')
  setImageSource(getRandFromArr(imageSrc[dice2 - 1]), '2')
  if (dice1 == 1 && dice2 == 1) {
    setDec('Snake Eyes!')
  } else if (dice1 == 6 && dice2 == 6) {
    setDec('Box Car!')
  }
}
function setImageSource(src, num) {
  const image = document.getElementById('main-image-' + num);
  image.src = src;
}
function getRandom() {
  return Math.floor(Math.random() * 6) + 1;
}
function getRandFromArr(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}
function setDec(txt) {
  document.getElementById('txt').innerHTML = txt;
}
