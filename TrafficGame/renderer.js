// Requiring Dependencies
const { ipcRenderer } = require('electron');
const Gameify = require('gameify')
const config = require('./config.json')
global.exit = function () {
  ipcRenderer.send('close-me')
}

const Game = global.game = new Gameify.Game('#main', {
	width: config.width,
	height: config.height
}, {
	tick: 1
});
const Background = require('./loadBackground')(Game, config)
Game.setView('col', '#568c49');

const Car = new Gameify.GameObject(() => {
	if (Car.Y === 255 && TrafficLight1Y1.status === 'stopped') {
		Car.status = 'stopped'
	} else {
		Car.status = 'go'
		Car.setXY(Car.X, Car.Y - 1)
		if (Car.Y === 0) Car.setXY(Car.X, config.height)
	}
}, 'car')
const Car2 = new Gameify.GameObject(() => {
	if (Car2.Y === 90 && TrafficLight1Y2.status === 'stopped') {
		Car.status = 'stopped'
	} else {
		Car2.status = 'go'
		Car2.setXY(Car2.X, Car2.Y + 1)
		if (Car2.Y === 440) Car2.setXY(Car2.X, 0)
	}

}, 'car')
const Car3 = new Gameify.GameObject(() => {
	if (Car3.X >= config.width - 70 && TrafficLight1X1.status === 'stopped') {
		Car3.status = 'stopped'
	} else {
		Car3.status = 'go'
		Car3.setXY(Car3.X + 1, Car3.Y)
		if (Car3.X === config.width - 60) Car3.setXY(0, Car3.Y)
	}
}, 'car')
const Car4 = new Gameify.GameObject(() => {
	if (Car4.X === 120 && TrafficLight1X2.status === 'stopped') {
		Car4.status = 'stopped'
	} else {
		Car4.status = 'go'
		Car4.setXY(Car4.X - 2, Car4.Y)
		if (Car4.X === 0) Car4.setXY(config.width, Car4.Y)
	}
}, 'car')

const TrafficLight1Y1 = new Gameify.GameObject(() => {
	if (TrafficLight1Y1.status === 'stopped') {
		TrafficLight1Y1.setView('col', 'red')
	} else {
		TrafficLight1Y1.setView('col', 'green')
	}
}, 'traffic-light')
const TrafficLight1Y2 = new Gameify.GameObject(() => {
	if (TrafficLight1Y2.status === 'stopped') {
		TrafficLight1Y2.setView('col', 'red')
	} else {
		TrafficLight1Y2.setView('col', 'green')
	}
}, 'traffic-light')
const TrafficLight1X1 = new Gameify.GameObject(() => {
	if (TrafficLight1X1.status === 'stopped') {
		TrafficLight1X1.setView('col', 'red')
	} else {
		TrafficLight1X1.setView('col', 'green')
	}
}, 'traffic-light')
const TrafficLight1X2 = new Gameify.GameObject(() => {
	if (TrafficLight1X2.status === 'stopped') {
		TrafficLight1X2.setView('col', 'red')
	} else {
		TrafficLight1X2.setView('col', 'green')
	}
}, 'traffic-light')

Game.addObject(Car);
Game.addObject(Car2);
Game.addObject(Car3)
Game.addObject(Car4)
Game.addObject(TrafficLight1Y1);
Game.addObject(TrafficLight1Y2);
Game.addObject(TrafficLight1X1);
Game.addObject(TrafficLight1X2);
TrafficLight1Y1.setSize(100, 5).setXY(20, 250).setView('col', 'grey').status = 'stopped';
TrafficLight1Y2.setSize(100, 5).setXY(20, 145).setView('col', 'grey').status = 'stopped';
TrafficLight1X1.setSize(5, 100).setXY(15, 150).setView('col', 'grey').status = 'go';
TrafficLight1X2.setSize(5, 100).setXY(120, 150).setView('col', 'grey').status = 'go';
Car.setSize(40, 60).setXY(25, config.height).setView('col', 'yellow')
Car2.setSize(40, 60).setXY(75, 0).setView('col', 'yellow')
Car4.setSize(60, 40).setXY(config.width, 205).setView('col', 'yellow')
Car3.setSize(60, 40).setXY(0, 155).setView('col', 'yellow')

Game.setCollisionFunc((obj1, obj2) => {
	if (obj1.class === 'car' &&
		  obj2.class === 'car') {
		for (var i = Game.objects.length - 1; i >= 0; i--) {
			Game.objects[i].delete();
		}
		const end = new Gameify.GameObject(() => {}, 'endScreen')
		Game.addObject(end);
		console.log('ded')
		end.setView('col', 'black').setSize(config.width, config.height);
	}
})

document.addEventListener('keydown', ev => {
	console.log(ev)
	if (ev.key === ' ') {
		for (var i = Game.objects.length - 1; i >= 0; i--) {
			if (Game.objects[i].class === 'traffic-light') {
				Game.objects[i].status = (Game.objects[i].status === 'stopped') ? 'go' : 'stopped';
			}
		}
	}
})
Game.start()
