const Gameify = require('gameify');
module.exports = (Game, config) => {
	const Background = [];

	Background.push(new Gameify.GameObject(() => {	
	}, 'road'))
	Game.addObject(Background[0])
	Background[0].setSize(100, config.height).setView('col', 'black').setXY(20, 0)

	Background.push(new Gameify.GameObject(() => {	
	}, 'road'))
	Game.addObject(Background[1])
	Background[1].setSize(config.width, 100).setView('col', 'black').setXY(0, 150)

	return Background;
}