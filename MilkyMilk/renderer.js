// Requiring Dependencies
const { ipcRenderer } = require('electron');
let clearToAlert = true;
function calculate() {
  const milkInputObj = document.getElementById('milkAmount'),
    cheeseInputObj = document.getElementById('cheeseAmount'),
    moneyInputObj = document.getElementById('tenderedInput'),
    outputCheese = document.getElementById('outputCheese'),
    outputMilk = document.getElementById('outputMilk'),
    outputChange = document.getElementById('outputChange'),
    outputTotal = document.getElementById('outputTotal');
  let milkAmount,
    cheeseAmount,
    tendered;
  try {
    milkAmount = parseInt(milkInputObj.value);
    cheeseAmount = parseInt(cheeseInputObj.value);
    tendered = parseFloat(moneyInputObj.value)
    if (Number.isNaN(milkAmount) || Number.isNaN(cheeseAmount) || Number.isNaN(tendered)) throw new Error('Number not allowed.')
  } catch (e) {
    console.log(e);
    if (!clearToAlert) return;
    clearToAlert = false;
    return alert('An input is invalid and/or missing.x')
  }
  const milkVal = round(milkAmount * 1.80),
    cheeseVal = round(cheeseAmount * 4.80),
    totalVal = round(milkAmount * 1.80 + cheeseAmount * 4.80),
    change = round((tendered - totalVal));

  outputCheese.innerHTML = '$' + cheeseVal;
  outputMilk.innerHTML = '$' + milkVal;
  outputTotal.innerHTML = '$' + totalVal;
  outputChange.innerHTML = '$' + change;

  if (change <= 0) outputChange.innerHTML = 'N/A';

  clearToAlert = true;
}
function clear() {
  document.getElementById('milkAmount').value = 0
  document.getElementById('cheeseAmount').value = 0

  document.getElementById('outputMilk').innerHTML = '$0.00'
  document.getElementById('outputCheese').innerHTML = '$0.00'
  document.getElementById('outputTotal').innerHTML = '$0.00'

}
function exit() {
  ipcRenderer.send('close-me')
}
function round(num) {
  return num.toFixed(2)
}
function start() {
  document.getElementById('clear').addEventListener('click', clear)
  document.getElementById('exit').addEventListener('click', exit)
  document.getElementById("milkAmount").addEventListener("change", calculate)
  document.getElementById("cheeseAmount").addEventListener("change", calculate)
  clear();
  setInterval(calculate, 200)
}
start()
