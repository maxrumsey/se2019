// Requiring Dependencies
const { ipcRenderer, Menu } = require('electron');
const axios = require('axios')
let APIKEY;

async function calculate() {
  const input = document.getElementById('input').value,
    output = document.getElementById('output'),
    conversion = document.getElementById('conversion').value,
    button = document.getElementById('calculate')
  let inputval = parseFloat(input),
    data;
  button.disabled = true;
  setSymbol(conversion);
  if (Number.isNaN(inputval)) {
    button.disabled = false;
    return alert('Input is not a number.')
  }
  try {
    data = await getRates(inputval);
    inputval = data.balance;
  } catch (e) {
    console.log(e);
    button.disabled = false;
    return alert('Fatal Error! Request failed. Info: ' + e.message)
  }

  output.innerHTML = round(data.rates[conversion] * inputval);
  button.disabled = false;
}
function clear() {
  document.getElementById('output').innerHTML = '0.00'
  document.getElementById('input').value = '0.00'

}
function exit() {
  ipcRenderer.send('close-me')
}
function round(num) {
  return num.toFixed(2)
}
document.getElementById('calculate').addEventListener('click', calculate)
document.getElementById('clear').addEventListener('click', clear)
document.getElementById('exit').addEventListener('click', exit)

// Fetches the rates from the API and converts the input to EUD.
async function getRates(balance) {
  const APIKEY = document.getElementById('APIKEY').value,
    res = await axios.get(`http://data.fixer.io/api/latest?access_key=${APIKEY}&format=1`)
  console.log(res);
  res.data.balance = balance / res.data.rates.AUD
  return res.data;
}
function setSymbol(cur) {
  const symboloutput = document.getElementById('symbol');
  let symbol;
  switch (cur) {
    case 'USD':
      symbol = '$'
      break;

    case 'GBP':
      symbol = '£'
      break;
    case 'CAD':
      symbol = '$'
      break;
    case 'INR':
      symbol = ''
      break;
    case 'NZD':
      symbol = '$'
      break;

  }
  symboloutput.innerHTML = symbol;
}
