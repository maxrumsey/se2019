// Requiring Dependencies
const { ipcRenderer } = require('electron');
var v;
const flavourMainEnum = {
  Chocolate: 0,
  Vanilla: 1,
  Strawberry: 2,
  Pistachio: 3
}
const flavourSauceEnum = {
  HotChoc: 0,
  Straw: 1,
  Caramel: 2
}
const amountSauceEnum = {
  none: 0,
  Sm: 1,
  Med: 2,
  Large: 3
}
global.exit = function () {
  ipcRenderer.send('close-side', data)
}
document.addEventListener('DOMContentLoaded', start)
function start() {
  ipcRenderer.send('give-info')
  ipcRenderer.once('data', (ev, data) => {
    global.data = data;
    console.log(data)
    dataLoaded(data);
  })
}
global.run = function() {
  killElements()
  document.getElementById('exit-button').disabled = false;
  const container = document.getElementById('container').value;
  const flavour = document.getElementById('flavour').value;
  const toppings = buildForms(container == 'Cup');
  console.log(toppings)
  buildElements({
    container,
    flavour,
    double: false,
    toppings
  })
}
function dataLoaded(data) {
  if (data) {
    const container = document.getElementById('container');
    const flavour = document.getElementById('flavour');
    const sauceFlavour = document.getElementById('cup-sauce-flavour');
    const amount = document.getElementById('cup-sauce-amount');

    if (data.container === 'Cone' && !data.double) {
      container.selectedIndex = 0
      if (data.toppings.cone.nuts) document.getElementById('cone-nut-form').click()
      if (data.toppings.cone.choc) document.getElementById('cone-choc-form').click()
    } else if (data.container === 'Cone' && data.double) {
      container.selectedIndex = 1
      if (data.toppings.cone.nuts) document.getElementById('cone-nut-form').click()
      if (data.toppings.cone.choc) document.getElementById('cone-choc-form').click()
    } else {
      container.selectedIndex = 2
      if (data.toppings.cup.nuts) document.getElementById('cup-nut-form').click()
      sauceFlavour.selectedIndex = flavourSauceEnum[data.toppings.cup.sauce]
      amount.selectedIndex = amountSauceEnum[data.toppings.cup.amount]


    }
    flavour.selectedIndex = flavourMainEnum[data.flavour]
  }
  run()
}
function killElements() {
  v = document.getElementById('viewer')
  for (let i = 0; i < v.children.length; i++) {
    v.children[i].style.display = 'none';
  } 
}

function buildElements(item) {
  let list = [];
  let total = 0;
  if (item.container == 'ConeDub') {
    item.container = 'Cone';
    item.double = true;
  }
  const container = document.getElementById(item.container.toLowerCase());
  container.src = `img/${item.container}s/${item.flavour}.gif`
  container.style.display = 'block';
  if (item.double) {
    const scoop = document.getElementById('scoop');
    scoop.src = `img/Scoops/${item.flavour}.gif`
    scoop.style.display = 'block';
  }
  if (item.double & item.container === 'Cone') {
    list.push({name: 'Double Scoop', cost: 2})
    if (item.toppings.cone.choc) list.push({name: 'Choc Top', cost: .75})
    if (item.toppings.cone.nuts) list.push({name: 'Nuts', cost: .5})
    document.getElementById('topping-cone-choc-2').style.display = ( (item.toppings.cone.choc) ? "block" : "none" );
    document.getElementById('topping-cone-nut-2').style.display = ( (item.toppings.cone.nuts) ? "block" : "none" );
  } else if (item.container === 'Cone') {
    list.push({name: 'Single Scoop', cost: 1.5})
    if (item.toppings.cone.choc) list.push({name: 'Choc Top', cost: .75})
    if (item.toppings.cone.nuts) list.push({name: 'Nuts', cost: .5})
    document.getElementById('topping-cone-choc-1').style.display = ( (item.toppings.cone.choc) ? "block" : "none" );
    document.getElementById('topping-cone-nut-1').style.display = ( (item.toppings.cone.nuts) ? "block" : "none" );
  } else if (item.container === 'Cup') {
    list.push({name: 'Bucket', cost: 3})
    if (item.toppings.cup.nuts) list.push({name: 'Nuts', cost: .5})
    document.getElementById('topping-bucket-nut').style.display = ( (item.toppings.cup.nuts) ? "block" : "none" );

    if (item.toppings.cup.amount !== 'none') {
      if (item.toppings.cup.amount == 'Sm') list.push({name: 'Small Sauce', cost: .5})
      else if (item.toppings.cup.amount == 'Med') list.push({name: 'Medium Sauce', cost: 1})
      else if (item.toppings.cup.amount == 'Large') list.push({name: 'Large Sauce', cost: 1.5})
      else alert('Error calculating sauce cost.')
      document.getElementById('topping-juice').style.display = 'block';
      document.getElementById('topping-juice').src = 'img/Toppings/' + item.toppings.cup.sauce + item.toppings.cup.amount + '.gif'
    } else {
      document.getElementById('topping-juice').style.display = 'none';
    }

  }
  document.getElementById("itemList").innerHTML = ''
  for (var i = 0; i < list.length; i++) {
    const node = document.createElement("LI");                 // Create a <li> node
    const textnode = document.createTextNode(`${list[i].name}: $${list[i].cost.toFixed(2)}`);         // Create a text node
    node.appendChild(textnode);                              // Append the text to <li>
    document.getElementById("itemList").appendChild(node); 

    total += list[i].cost
  }
  document.getElementById('total').innerHTML = `$${total.toFixed(2)}`
    document.getElementById('subtotal').innerHTML = `$${(total / 10 * 9).toFixed(2)}`
        document.getElementById('gst').innerHTML = `$${(total / 10).toFixed(2)}`
  item.cost = total;
  global.data = item;


}
function buildForms(bucket) {
  if (bucket) {
    document.getElementById('cup-form').style.display = 'block'
    document.getElementById('cone-form').style.display = 'none'

    const sauce = document.getElementById('cup-sauce-flavour').value;
    const amount = document.getElementById('cup-sauce-amount').value;
    const nuts = document.getElementById('cup-nut-form').checked;
    
    const obj = {
      cup: {
        sauce,
        amount,
        nuts
      }
    }
    return obj;
  } else {
    document.getElementById('cup-form').style.display = 'none'
    document.getElementById('cone-form').style.display = 'block'

    const choc = document.getElementById('cone-choc-form').checked;
    const nuts = document.getElementById('cone-nut-form').checked;

    const obj = {
      cone: {
        choc,
        nuts
      }
    }
    return obj;
  }
}
setTimeout(run, 1000)