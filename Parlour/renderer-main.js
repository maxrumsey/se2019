// Requiring Dependencies
const { ipcRenderer } = require('electron');
global.items = [];
global.exit = function () {
  ipcRenderer.send('close-me')
}
global.launchNew = function () {
  open({})
}
function open(data) {
  ipcRenderer.send('launch-side', data);
  ipcRenderer.once('return-data', (ev, data) => {
    global.items.push(data)
    run()
  })
}
global.deleteAll = function() {
  global.items = [];
  run();
}
global.run = function() {
  const list = document.getElementById('ordersList')
  let total = 0;
  list.innerHTML = '';
  if (items.length === 0) {
    const node = document.createElement("LI");                 // Create a <li> node
    const textnode = document.createTextNode(`No items in list.`);         // Create a text node
    node.appendChild(textnode);                              // Append the text to <li>
    list.appendChild(node); 
  }
  for (var i = 0; i < items.length; i++) {
    console.log(items[i])
    list.innerHTML += `<li>${genCost(items[i])}: $${items[i].cost.toFixed(2)} <button onclick='remove(${i})'>Delete</button> <button onclick='edit(${i})'>Edit</button></li>`
    total += items[i].cost;
  }
  document.getElementById('total').innerHTML = '$' + total.toFixed(2);
  document.getElementById('gst').innerHTML = '$' + (total / 10).toFixed(2);
  document.getElementById('subtotal').innerHTML = '$' + (total / 10 * 9).toFixed(2);

  const cashin = parseFloat(document.getElementById('tendered').value);
  if (isNaN(cashin)) return alert('Tendered is not valid.');
  const change = (cashin - total);
  document.getElementById('change').innerHTML = '$' + change.toFixed(2);
  if (change < 0) document.getElementById('change').style.color = 'red';
  else document.getElementById('change').style.color = 'black';
}
global.remove = function(id) {
  global.items.splice(id, 1);
  return run()
}
global.edit = function(id) {
  open(global.items[id])
  global.remove(id)
}
function genCost(item) {
  if (item.double && item.container === 'Cone') return (`${item.flavour} Double Cone`)
  if (item.container === 'Cup') return (`${item.flavour} Bucket`)
  else return (`${item.flavour} Cone`)
}
setTimeout(run, 150)
  /*
  {
    container: 'Cup',
    flavour: 'Strawberry',
    double: false,
    toppings: {
      cup: {
        nuts: true,
        sauce: 'HotChoc',
        amount: 'Sm'
      }
    }
  }
  */