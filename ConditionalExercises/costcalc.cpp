#include <iostream>
#include <string.h>
int main() {
	std::cout << "Price Calculator -- Max Rumsey 2019 SE" << std::endl;

	int age = -1;
	int price;
	std::cout << "Enter your age: ";
	std::cin >> age;
	
	if (age == -1) {
		std::cout << "Malformed input" << std::endl;
		return 1;
	}

	if (age >= 18 && age <= 65) {
		price = 35;
	} else {
		price = 20;
	}

	std::cout << "The cost will be $" << price << ".00" << std::endl;
	return 0;
}