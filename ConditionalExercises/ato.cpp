#include <iostream>
#include <string.h>
int main() {
	std::cout << "ATO Rebate Calculator -- Max Rumsey 2019 SE" << std::endl;

	float km = -1;
	int cc = -1;
	float claimed = -1;
	std::cout << "Enter the engine capacity of the vehicle: ";
	std::cin >> cc;
	std::cout << "Enter the KMs travelled this year: ";
	std::cin >> km;
	
	if (cc >= 1500) {
		claimed = 0.65;
	} else {
		claimed = 0.45;
	}

	std::cout << "You travelled " << km << "km this year and earnt $" << km * claimed << " back." << std::endl;
	std::cout << "With a cc of " << cc << "cc and a rate of claimage of " << claimed << " cents per kilometre." << std::endl;
	return 0;
}