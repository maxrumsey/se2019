#include <iostream>
#include <string.h>
int main() {
	std::cout << "Entrace Calculator -- Max Rumsey 2019 SE" << std::endl;

	int age = -1;

	std::cout << "Enter your age: ";
	std::cin >> age;
	
	if (age == -1) {
		std::cout << "Malformed input" << std::endl;
		return 1;
	}

	if (age >= 18) {
		std::cout << "Entrance allowed." << std::endl;
	} else {
		std::cout << "Entrance not allowed." << std::endl;
	}
	return 0;
}