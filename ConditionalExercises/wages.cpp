#include <iostream>
#include <string.h>

int main() {
  std::cout << "Wage Calculator" << std::endl;
  
  int age;
  float hours;
  std::string name;

  std::cout << "Enter Your Name: " << std::endl; 
  std::cin >> name;

  std::cout << "Enter Your Age: " << std::endl; 
  std::cin >> age;

  std::cout << "Enter Your Hours Worked This Week: " << std::endl; 
  std::cin >> hours;

  float wage;

  if (age < 16) {
  	wage = 5.60;
  } else {
  	wage = 7.70;
  }

  float earned = wage * hours;

 	std::cout << name << ", you earnt $" << earned << " this week. You worked " << hours << " hour(s). Good job!" << std::endl;
  return 0;
}